<?php

namespace App\Console\Commands;

use App\Weather;
use Illuminate\Console\Command;

class WeatherSavingTemperatureCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'weather:temperature';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weather Saving Temperature Command';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'http://api.openweathermap.org/data/2.5/weather?q=' . env('OPENWEATHERMAP_CITY') . '&units=metric&appid=' . env('OPENWEATHERMAP_APIKEY'));

        $statusCode = $res->getStatusCode();
        $body = (string) $res->getBody();
        $bodyArr = json_decode($body, true);

        if($statusCode == '200') {
            Weather::create([
                'city' => env('OPENWEATHERMAP_CITY'),
                'temperature' => data_get($bodyArr, 'main.temp'),
                'dump' => $body
            ]);

            $status = 'Temperature stored in base';
        } else {
            $status = 'Error receiving temperature';
        }

        $this->info($status);
    }
}
