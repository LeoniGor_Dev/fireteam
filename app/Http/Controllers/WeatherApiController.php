<?php

namespace App\Http\Controllers;

use App\Http\Requests\WeatherApiRequest;
use App\Weather;
use Illuminate\Http\Request;

class WeatherApiController extends Controller
{
    public function weather(WeatherApiRequest $request)
    {
        $xToken = $request->header('x-token');

        if($xToken != env('X_TOKEN')) {
            return response()->json([
                'result' => 'fail_token',
                'message' => 'wrong token key',
                'data' => []
            ]);
        }

        $day = $request->input('day');
        $dayFrom = date('Y-m-d 00:00:00', strtotime($day));
        $dayTo = date('Y-m-d 23:59:59', strtotime($day));

        $weather = Weather::select(['city', 'temperature', 'created_at'])->whereBetween('created_at', [$dayFrom, $dayTo])->get();
        $count = count($weather);

        if(!empty($weather) && $count > 0) {
            $result = 'success';
            $message = 'Records found: ' . $count;
        } else {
            $result = 'fail';
            $message = 'No results from the query were found';

        }

        return response()->json([
            'result' => $result,
            'message' => $message,
            'data' => $weather
        ]);
    }
}
